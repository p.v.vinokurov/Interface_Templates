using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_WeaponListController : MonoBehaviour
{
    public GameObject pfb_InventorySlot;
    public RectTransform contentScrollView;

    GameObject go;

    // Start is called before the first frame update
    void Start()
    {
        if(pfb_InventorySlot)
        {
            go = Instantiate(pfb_InventorySlot, new Vector3(0, 0, 0), Quaternion.identity);
        }

        if(contentScrollView && go)
        {
            go.transform.SetParent(contentScrollView);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
