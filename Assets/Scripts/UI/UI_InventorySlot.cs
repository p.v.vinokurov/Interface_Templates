using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UI_InventorySlot : MonoBehaviour, IDropHandler
{
    public void OnDrop(PointerEventData eventData)
    {
        Transform otherItemTransform = eventData.pointerDrag.transform; // �������� transform ��������
        otherItemTransform.SetParent(transform); // ������ transform ����� ������������ ��� ��������
        otherItemTransform.localPosition = Vector3.zero; // ����������� ������� � �����
    }
}
