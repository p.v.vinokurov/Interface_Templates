using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UI_InventoryItem : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    RectTransform _rectTransform;
    Canvas _canvas;
    CanvasGroup _canvasGroup;
    Mask _scrollvewMask;

    void Start()
    {
        _rectTransform = GetComponent<RectTransform>();
        _canvas = GetComponentInParent<Canvas>();
        _canvasGroup = GetComponent<CanvasGroup>();
        _scrollvewMask = GetComponentInParent<Mask>();
    }
    public void OnBeginDrag(PointerEventData eventData)
    {
        _canvasGroup.blocksRaycasts = false; // ������� �������� ������ ����, ����� ������� � ����
        _scrollvewMask.enabled = false; // ��������� ����� �������� ScrollView �� ����� ����������� ��������
    }

    public void OnDrag(PointerEventData eventData)
    {
        _rectTransform.anchoredPosition += eventData.delta / _canvas.scaleFactor;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        transform.localPosition = Vector3.zero;
        _canvasGroup.blocksRaycasts = true; // �������� ������� �������, ����� ����� ���� ����������
        _scrollvewMask.enabled = true; // �������� ����� �������� ScrollView �������
    }
}
