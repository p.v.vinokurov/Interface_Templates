using System.Collections;
using UnityEngine;

public class BattleManager : ScriptableObject
{
    private ArrayList battleQueue = new ArrayList();    // ������������ ������� ���

    private int roundCount = 1;                         // ������� �������
    private int cutOfRound = 0;                         // ������� ������

    private int enemyCount = 0;                         // ���-�� ������ � ������� ���
    private int playerCount = 0;                        // ���-�� ���������� ������ � ������� ���

    private static BattleManager instance;

    public BattleManager()
    {
        if(instance != null)
        {
            UnityEngine.Debug.LogWarning("Tried to generate more than one instance of singleton BattleManager");
            return;
        }

        instance = this;
    }
    public static BattleManager Instance
    {
        get
        {
            if(instance == null)
            {
                ScriptableObject.CreateInstance<BattleManager>();
            }
            return instance;
        }
    }
}
