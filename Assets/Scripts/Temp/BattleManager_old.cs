﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using SickDev.CommandSystem;

public class BattleManager : ScriptableObject
{
    private ArrayList battleQueue = new ArrayList();                    // Динамическая очередь боя
    private ArrayList destroyedObjects = new ArrayList();               // Копия очереди боя перед его началом. Используется для уничтожения всех объектов со сцены после окончания боя
    private List<GameObject> destroyedPrefabs = new List<GameObject>(); // Массив хранит трупы уничтоженных в бою, чтобы в методе ClearBattlefield удалить их
    private List<IRegularble> playersAndFremensList = new List<IRegularble>();   // Список игроков в бою. Используется врагами для поиска цели
    private List<IRegularble> enemiesList = new List<IRegularble>();			 // Список врагов в бою (кроме Фременов). Используется Фременами для атаки врагов и Игроком для проверки наличия препятсвий для стрельбы
    //private List<Enemy> fremensList = new List<Enemy>();				// Список Фременов. Его используют враги
    int roundCount = 1;                                                 // Счетчик раундов
    int cutOfRound = 0;                                                 // Отсечка раунда
    int enemyCount = 0;                                                 // Количество врагов участников боя
    int playerMechCount = 0;                                            // Количество мехов игрока
    int freemanCount = 0;                                               // кол-во фрименов
    private List<Enemy> deadEnemyList = new List<Enemy>();              // список мертвых противников, нужен для получения лута
    public List<PlayerMech> allPlayerMechList = new List<PlayerMech>(); // список всех плеермехов, нужен чтобы потом подготовить их к окончанию боя
    public List<Mech> mechSnapShots = new List<Mech>(); // исходные состояния мехов до начала боя


    bool stepIsEnding = false;
    //Loots
    int fuelLoot = 0;
    int scrapLoot = 0;
    int ammoLoot = 0;
    int rocketLoot = 0;
    List<Module> moduleLootList = new List<Module>();

    public bool battleIsFinishing = false; //битва в процессе завершения, флажок для отмены множественного вызова завершения

    #region Singleton Implementation

    private static BattleManager _instance;

    public BattleManager()
    {
        // this function will be called whenever an instance of the SpawnController class is made
        // first, we check that an instance does not already exist (this is a singleton, afterall!)
        if (_instance != null)
        {
            // drop out if instance exists, to avoid generating duplicates
            Debug.LogWarning("Tried to generate more than one instance of singleton LevelManager");
            return;
        }

        // as no instance already exists, we can safely set instance to this one
        _instance = this;
    } // синглтон

    public static BattleManager Instance
    {
        get
        {
            if (_instance == null)
            {
                ScriptableObject.CreateInstance<BattleManager>();
            }
            return _instance;
        }
    } // синглтон

    /// <summary>
    /// Ensure that the instance is destroyed when the game is stopped in the editor.
    /// </summary>
    void OnApplicationQuit()
    {
        _instance = null;
    }
    #endregion

    /// <summary>
    /// Подготовка очереди для следующего раунда
    /// </summary>
    private void PrepareBattleQueueForNextRound()
    {
        roundCount += 1;
        // Конец боя Заглуха
#pragma warning disable CS1030 // #warning: "заглушка'
#warning заглушка
        //TODO: (Винокуров) Возможно эта проверка здесь не нужна, потому что есть проверка в RemoveObjectFromBattleQueue
      /*  if (DetermineEndBattle())
#pragma warning restore CS1030 // #warning: "заглушка'
        {
            if (!battleIsFinishing) //препятствуем множественному вызову
            {
                EndBattle();
            }
            return;
        }*/
        this.battleQueue.Sort();
        cutOfRound = battleQueue.Count;     // Задаем отсечку раунда
        CalculateEnemyAndPlayerCount();
        F3DTime.time.AddTimer(0.5f, 1, Step);  // Первый ход, следующего раунда!                 
    }

    // Метод удаляет со сцены танки победивших и груды металла от побежденных
    public void ClearBattlefieldForSimulator()
    {
        UnityEngine.Debug.Log("Battle Manager: Кол-во живых объектов, которые будут уничтожены = " + destroyedObjects.Count);
        foreach (IRegularble regular in destroyedObjects)
        {
            regular.SelfDestroyFromBattleSimulator();
        }

        // Удаляем со сцены объекты уничтоженных танков
        UnityEngine.Debug.Log("Battle Manager: Кол-во трупов, которые будут уничтожены = " + destroyedPrefabs.Count);
        foreach (GameObject go in destroyedPrefabs)
        {
            Destroy(go, 2.0f);
        }
        battleIsFinishing = false;
        LevelHandler.Instance.ResetLinesAndMarkers(); // Стираем линии (Область перемещения и радиусы стрельбы)
    }

    // Метод удаляет со сцены танки победивших и груды металла от побежденных
    private void ClearBattlefiled()
    {
        UnityEngine.Debug.Log("Battle Manager: Кол-во живых объектов, которые будут уничтожены = " + destroyedObjects.Count);
        foreach (IRegularble regular in destroyedObjects)
        {    
            regular.SelfDestroy();
        }

        // Удаляем со сцены объекты уничтоженных танков
        UnityEngine.Debug.Log("Battle Manager: Кол-во трупов, которые будут уничтожены = " + destroyedPrefabs.Count);
        foreach (GameObject go in destroyedPrefabs)
        {
            Destroy(go, 3.0f);
        }
        LevelHandler.Instance.ResetLinesAndMarkers(); // Стираем линии (Область перемещения и радиусы стрельбы)
    }

    private void UseAfterBattlePerks()
    { 
        foreach (Mech _mech in PlayerStats.Instance.mechList)
        {
            if (_mech.readyForBattle) // этот мех был готов к бою, соответсвенно в нем учавствовал //TODO: проверить сохранился ли этот флаг
            {
                        foreach (PerkData _perk in _mech.character.perkList)
                        {
                            if (_perk.type == PerkType.basicType.afterBattle)
                            {
                                BuffsDB.Instance.UsePerk(_perk.iD,PerkType.specialType.special, _mech.character, _mech, BuffsDB.Instance.perkDB.perkList[_perk.iD].modArray);
                            }
                        }
            }
        }
    }
    /* //залупа, написано неподумав
    private void UpdateCharStats()
    {
        foreach (Mech _mech in PlayerStats.Instance.mechList)
        {
            if (_mech.character != null)
            {
                if (_mech.character.currentHP <= 0) _mech.character.currentHP = 1;
            }
        }
    }
    */

    public void PrepareTurotialBattleEnding() //подготавливаем окончание боя
    {
        //загружаем асинхронно сцену с картой
        BattleInterfaceManager.Instance.playerMechGUIHandler.currentCamera.BattleComplete();
        BattleInterfaceManager.Instance.playerMechGUIHandler.currentCamera.EnablePlayerInput(false);
        //загрузить стартовое состояние плеерстатс
    }


    private void EndBattle()
    {
        battleIsFinishing = true;
        BattleInterfaceManager.Instance.playerMechGUIHandler.currentCamera.BattleComplete();
        BattleInterfaceManager.Instance.playerMechGUIHandler.currentCamera.EnablePlayerInput(false);

     //   allPlayerMechList.Clear(); // считаем мехов на конец боя

 /*       foreach (object obj in battleQueue) // считаем сколько всего осталось мехов
        {
            if (obj is PlayerMech)
            {
                allPlayerMechList.Add(obj as PlayerMech);
            }
        }
        */
        foreach (PlayerMech _playerMech in allPlayerMechList) //производим подготовительные мероприятия мехов для окончания боя, тушим пожары
        {
            _playerMech.PrepareForEndBattle(); 
        }
        if (enemyCount > 0 && playerMechCount == 0) // противники есть а игроков нет
        {
             LoseGame();
        }
        if (enemyCount == 0 && playerMechCount > 0) //противников нет а игроки есть
        {
            WinBattle();
        }
        //отчищаем список противников на карте
        PlayerStats.Instance.globalMap.sectorList[PlayerStats.Instance.currentSectorID].pointList[PlayerStats.Instance.currentPointID].enemiesList.Clear();

        if (!GameOptions.Instance.debugMode) // сохраняем игру если не в дебаг моде
        {
            GameSaverLoader.SaveGame();
        }

    }
    public List<Module> CountModulesLoot(List<Enemy> _enemyList) // расчет лута после боя
    {
        List<Module> modulesList = new List<Module>();
        foreach (Enemy _enemy in _enemyList)
        {
            List<Module> tmpList = new List<Module>(); // временный список для случайного выбора модулей из танка
            foreach (Module _module in _enemy.currentStats.engineList)
            {
                tmpList.Add(_module);
            }
            foreach (Module _module in _enemy.currentStats.weaponList)
            {
                tmpList.Add(_module);
            }
            foreach (Module _module in _enemy.currentStats.armorList)
            {
                tmpList.Add(_module);
            }
            foreach (Module _module in _enemy.currentStats.shieldList)
            {
                tmpList.Add(_module);
            }
            foreach (Module _module in _enemy.currentStats.generatorList)
            {
                tmpList.Add(_module);
            }
            foreach (Module _module in _enemy.currentStats.enhancerList)
            {
                tmpList.Add(_module);
            }
            int _randomNumber = UnityEngine.Random.Range(0, tmpList.Count);
            if (tmpList.Count != 0)
            {
                modulesList.Add(tmpList[_randomNumber]);
            }
        }
        return modulesList;
    }
    private void MakeMechsSnapshots() //сохраняем исходные состояния мехов
    {
        mechSnapShots.Clear();
        int i = 0;
        foreach (PlayerMech _playerMech in allPlayerMechList)
        {
            _playerMech.mech.battleId = i; //раздаем айди в бою
            mechSnapShots.Add(_playerMech.mech.GetMechCopy());
            i++;
        }
    }
    private void WinBattle() //выигрываем битву
    {
        UseAfterBattlePerks(); //применяем перки после боя

        List<Character> _charLootList = new List<Character>();
        List<Mech> _mechLootList = new List<Mech>();
        List<Module> _lootList = new List<Module>();

        if (PlayerStats.Instance.currentEvent != null)
        {
            switch (PlayerStats.Instance.currentEvent.eventType)
            {
                case GlobalEvent.globalEventType.caravanFound:
                    {
                        PlayerStats.Instance.currrentCaravan = NewGameGenerator.GenerateLootCaravan(); //караван с лутом
                    }
                    break;
                case GlobalEvent.globalEventType.freemenAPCFound:
                    {
                        switch (freemanCount)
                        {
                            case 1: // случайно 1 из 3
                                break;
                            case 2: //случайно 2 из 3
                                break;
                            case 3: //персонаж, модуль, дроп
                                break;
                            default: //никакого лута
                                break;
                        }
                    }
                    break;
                case GlobalEvent.globalEventType.finalboss:
                    {
                        WinGame();
                    }
                    break;
                default: // ничего не добавляем
                    break;
            }
        }


        _lootList.AddRange(CountModulesLoot(deadEnemyList)); //считаем модули лута с противников
        deadEnemyList.Clear();


        int _avgAmmo = InventoryDB.Instance.gameBaseStats.avgAmmoFP;
        int _minAmmo = (int)(_avgAmmo - _avgAmmo / 2);
        int _maxAmmo = (int)(_avgAmmo + _avgAmmo / 2);
        int _ammoLoot = UnityEngine.Random.Range(_minAmmo, _maxAmmo);

        int _avgRockets = InventoryDB.Instance.gameBaseStats.avgRocketsFP;
        int _minRockets = (int)(_avgRockets - _avgRockets / 2);
        int _maxRockets = (int)(_avgRockets + _avgRockets / 2);
        int _rocketLoot = UnityEngine.Random.Range(_minRockets, _maxRockets);

        int _fuelLoot = 0; // времяночки


        int _avgScrap = InventoryDB.Instance.gameBaseStats.avgScrapFP;
        int _min = (int)(_avgScrap - _avgScrap / 2) * (PlayerStats.Instance.currentSectorID + 1);
        int _max = (int)(_avgScrap + _avgScrap / 2) * (PlayerStats.Instance.currentSectorID + 1);
        int _scrapLoot = UnityEngine.Random.Range(_min, _max);
        //TODO: Подсчитать лут


        if (PlayerStats.Instance.currrentCaravan != null) // значит сражались с караваном
        {
            Caravan currentCaravan = PlayerStats.Instance.currrentCaravan;
            switch (PlayerStats.Instance.currrentCaravan.type)
            {
                case Caravan.caravanType.moduleDrop:
                    {
                        _lootList.AddRange(currentCaravan.moduleLootList);
                    }
                    break;
                case Caravan.caravanType.charDrop:
                    {
                        _charLootList.AddRange(currentCaravan.charLootList);
                    }
                    break;
                case Caravan.caravanType.mechDrop:
                    {
                        _mechLootList.AddRange(currentCaravan.mechLootList);
                    }
                    break;
                case Caravan.caravanType.miscDrop:
                    {
                        _ammoLoot = currentCaravan.ammoLoot;
                        _rocketLoot = currentCaravan.rocketLoot;
                        _fuelLoot = currentCaravan.fuelLoot;
                        _scrapLoot = currentCaravan.scrapLoot;
                    }
                    break;
                default:
                    break;
            }
            //TODO: в зависимости от типа каравана показываем соответствующие ништяки
            PlayerStats.Instance.currrentCaravan = null; // обнуляем караван
            PlayerStats.Instance.globalEnemy.currentCaravan = null; // обнуляем караван
        }

        //       PlayerStats.Instance.GetCurrentSectorPoint().pointInventory.AddRange(_lootList); // добавляем в точку на землю
 //       PlayerStats.Instance.playerInventory.AddRange(_lootList);
 //       PlayerStats.Instance.characterList.AddRange(_charLootList);
        PlayerStats.Instance.mechList.AddRange(_mechLootList);
        PlayerStats.Instance.ammo += _ammoLoot;
        PlayerStats.Instance.rockets += _rocketLoot;
        PlayerStats.Instance.fuel += _fuelLoot;
        PlayerStats.Instance.scrap += _scrapLoot;

        if (PlayerStats.Instance.GetCurrentSectorPoint().castle == true) // убираем крепость с клетки, если она там была
        {
            PlayerStats.Instance.GetCurrentSectorPoint().castle = false;
        }
        if (!GameOptions.Instance.debugMode) // сохраняем игру если не в дебаг моде
        {
            GameSaverLoader.SaveGame();
        }
        //TODO: здесь переделать логику с завершением игры

        scrapLoot = _scrapLoot;
        fuelLoot = _fuelLoot;
        rocketLoot = _rocketLoot;
        ammoLoot = _ammoLoot;
        moduleLootList.Clear();
        moduleLootList.AddRange(_lootList);

        //ClearBattlefieldForSimulator();

        F3DTime.time.AddTimer(4,1, ShowWinBattleWidget);

    }

    public void PrepareMechsToExitFromBattle()
    {
        battleIsFinishing = false; 
        foreach (Mech _mech in PlayerStats.Instance.mechList)
        {
            if (_mech.character != null)
            {
                if (_mech.character.currentHP <= 0)
                {
                    _mech.character = null; //отправляем персонажа в небытие
                }
            }
        }
    }

    private void ShowWinBattleWidget()
    {

        BattleInterfaceManager.Instance.endBattleWidget.ShowWinBattleWidget(moduleLootList, scrapLoot, rocketLoot, ammoLoot, fuelLoot);
        DevConsole.singleton.Log("модулей получено" + moduleLootList.Count.ToString());
        battleIsFinishing = false;
    }   
    private void WinGame() //выигрываем игру
    {

        PlayerStats.Instance.newgame = true;
        BattleInterfaceManager.Instance.endBattleWidget.ShowPanel(EndBattleWidget.widgetStates.winGame,null);
    }
    private void LoseGame() //если проиграли в бою - то проиграли игру, получаем инфу об этом и выходим в главное меню
    {
        PlayerStats.Instance.newgame = true;
        BattleInterfaceManager.Instance.endBattleWidget.ShowPanel(EndBattleWidget.widgetStates.loseGame, null);
    }


    public bool CheckCanRetreat()
    {
        if (PlayerStats.Instance.currentPointID == 0) return false;
        if (PlayerStats.Instance.fuel <= 0) return false;
        return true;
    }
    public void RetreatFromBattle() // побег из боя
    {
        List<SectorPoint> _visitedPointList = new List<SectorPoint>();
        foreach (SectorPoint _sectorPoint in PlayerStats.Instance.globalMap.sectorList[PlayerStats.Instance.currentSectorID].pointList)
        {
            if (_sectorPoint.visited && _sectorPoint.iD != PlayerStats.Instance.currentPointID)
            {
                _visitedPointList.Add(_sectorPoint); // делаем списко возможных точек для побега
            }
        }
        List<SectorPoint> _pointListWithNoEnemies = new List<SectorPoint>();
        foreach (SectorPoint _sectorPoint in _pointListWithNoEnemies)
        {
            if (_sectorPoint.enemiesList.Count == 0 && !PlayerStats.Instance.globalEnemy.CheckPatrolInPoint(_sectorPoint) && !PlayerStats.Instance.globalEnemy.CheckCaravanInPoint(_sectorPoint))
            {
                _pointListWithNoEnemies.Add(_sectorPoint); // делаем списко возможных точек для побега
            }
        }
        if (_pointListWithNoEnemies.Count != 0) // идем в свободную точку
        {
            int _randomNumber = UnityEngine.Random.Range(0, _pointListWithNoEnemies.Count);
            PlayerStats.Instance.currentPointID = _randomNumber;
        }
        else
        {
            List<SectorPoint> _avaliablePointList = new List<SectorPoint>();
            foreach (SectorPoint _sectorPoint in PlayerStats.Instance.globalMap.sectorList[PlayerStats.Instance.currentSectorID].pointList)
            {
                if (!PlayerStats.Instance.globalEnemy.CheckPatrolInPoint(_sectorPoint) && !PlayerStats.Instance.globalEnemy.CheckCaravanInPoint(_sectorPoint)) // есть враги но нет каравана и патруля
                {
                    _avaliablePointList.Add(_sectorPoint);
                }
            }
            int _randomNumber = UnityEngine.Random.Range(0, _avaliablePointList.Count);
            //TODO: проверяем если нет таких точек без патруля и каравана, то ищем еще какую нибудь точку, или уничтожаем патруль
            PlayerStats.Instance.globalMap.sectorList[PlayerStats.Instance.currentSectorID].pointList[_randomNumber].enemiesList.Clear(); //убираем противников из точки
            PlayerStats.Instance.currentPointID = _randomNumber;
            //Проверяем есть ли в точке патруль и не выбираем его
        }
        PlayerStats.Instance.fuel -= 1;
        BattleInterfaceManager.Instance.endBattleWidget.ShowPanel(EndBattleWidget.widgetStates.retreat, null);

    }
    /// <summary>
    /// Метод передает право очередного хода объекту из очереди
    /// </summary>
    public void Step()
    {
        stepIsEnding = false;
        if (battleQueue.Count > 0)
        {
            ((IRegularble)battleQueue[0]).RunStep();
        }
        else
        {
            BattleInterfaceManager.Instance.playerMechGUIHandler.currentCamera.EnablePlayerInput(false);
            UnityEngine.Debug.Log("Конец боя! Step()");
        }
    }
    // Метод определяет кол-во врагов и мехов игрока
    private void CalculateEnemyAndPlayerCount()
    {
        enemyCount = 0;
        playerMechCount = 0;
        freemanCount = 0;
        playersAndFremensList.Clear();
        enemiesList.Clear();
        Enemy enemy;
        foreach (object obj in battleQueue)
        {
            if(obj is Enemy)
            { 
                enemy = obj as Enemy;
                if(!enemy.isFremen) // Если враг не Фремен
                {
                	enemiesList.Add(obj as Enemy); // Добавляем его в список
                	enemyCount += 1;			   
                }
                else
                {
                    freemanCount += 1;
                	playersAndFremensList.Add(obj as Enemy); // Фременов не считаем их кол-во не влияет на результат сражения
                }
            }
            if(obj is PlayerMech)
            {
                playerMechCount += 1;
                playersAndFremensList.Add(obj as IRegularble); 
            }
        }
    }
    /*
    public void CheckKilledMechsInBattleQueue() // проверяем каждый раз в fireanimationend()
    {
        List<Enemy> enemies = new List<Enemy>();
        List<PlayerMech> playerMeches = new List<PlayerMech>();

        foreach (object obj in battleQueue)
        {
            if (obj is Enemy)
            {
                enemies.Add(obj as Enemy);                           
            }
            if (obj is PlayerMech)
            {
                playerMeches.Add(obj as PlayerMech);
            }
        }
        int i = 0;
        while (i < enemies.Count)
        {
            if (enemies[i].CheckMechKilled())
            {
                RemoveObjectFromBattleQueue(enemies[i]);
            }
            else
            {
                i++;
            }
        }
        i = 0;
        while (i < playerMeches.Count)
        {
            if (playerMeches[i].CheckMechKilled())
            {
                RemoveObjectFromBattleQueue(playerMeches[i]);
            }
            else
            {
                i++;
            }
        }


    }
    */
    // Метод вызывается всякий раз, после удаления врага из очереди и после окнчания раунда
    private bool DetermineEndBattle()
    {
        if ((enemyCount == 0) || (playerMechCount == 0))        // Если врагов больше нет или игроков
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    #region Public Methods

    public void EndCurrentPlayerTurn()
    {
        if (stepIsEnding)
        {
            Debug.Log("повторный вызов конца хода");
            return;
        }
        stepIsEnding = true;
        Debug.Log("end of turn");
        // Сообщаем камере, что игрок закончил ход
        //TODO: (Винокуров) Отключение камеры
        //BattleInterfaceManager.Instance.currentCamera.EnablePlayerInput(false); 
        BattleInterfaceManager.Instance.playerMechGUIHandler.currentPlayerMech.DeselectCurrentWeapon();
        BattleInterfaceManager.Instance.playerMechGUIHandler.currentPlayerMech.SetIdleAnimationState();      // Переключаем анимационный контроллер юнита игрока в idle
        BattleInterfaceManager.Instance.playerMechGUIHandler.currentPlayerMech.ResetHead();                  // Перевод башни в походное состояние
        // Сообщаем левел хэндлеру, что необходимо стереть область передвижения
        LevelHandler.Instance.ResetLinesAndMarkers();
        EndOfStep();
        // ChangeStateToEnemy();
        //  GameInterfaceManager.Instance.storyCanvasManager.HideStoryBanners();

        //на всякий случай
        if (DetermineEndBattle()) // Бой закончен 
        {
            if (!battleIsFinishing) //препятствуем множественному вызову
            {
                EndBattle();
            }
        }
    }

    /// <summary>
    /// Удаление участника сражения из очереди
    /// </summary>
    /// <param name="removedObject">Участник на удаление</param>
    public bool RemoveObjectFromBattleQueue(object removedObject)
    {
    	Enemy enemy;
        if(removedObject is Enemy)
        {
            //Enemy removedEnemy = removedObject as Enemy;
#pragma warning disable CS1030 // #warning: "Завести бинарный поиск как-то не удалось'
#warning Завести бинарный поиск как-то не удалось
            // Бинарный поиск в отсортированном массиве
            //int i = BattleQueue.BinarySearch(tmpPm);

            // Итеративный поиск
            for(int i = 0; i < battleQueue.Count; i++)
#pragma warning restore CS1030 // #warning: "Завести бинарный поиск как-то не удалось'
            {
                if(removedObject.Equals(battleQueue[i]))    // Объект найден в очереди
                {
                	enemy = removedObject as Enemy;
                	if(!enemy.isFremen) // Если это не Фремен
                	{
                		deadEnemyList.Add(removedObject as Enemy); // добавляем протвника в список мертвых чтобы его потом разобрать на запчасти
                        battleQueue.RemoveAt(i);    // Удаление найденного объекта
                        enemyCount -= 1;            // На одного врага меньше
                	}
                	else // Если Фремен, то просто удаляем из очереди
                	{
                		battleQueue.RemoveAt(i);
                	}

#pragma warning disable CS1030 // #warning: "Здесь может закрасться ошибка. Надо все просчитать внимательно!!!'
#warning Здесь может закрасться ошибка. Надо все просчитать внимательно!!!
                    // Правим отсечку раунда, если удаляемый объект еще не выполнил свой ход в текущем раунде
                    if (i < cutOfRound)
#pragma warning restore CS1030 // #warning: "Здесь может закрасться ошибка. Надо все просчитать внимательно!!!'
                    {
                        cutOfRound -= 1;
                    }
                    break;
                }
            }
            // Обнуляем указатель
            //removedEnemy = null;
        }

        if(removedObject is PlayerMech)
        {
            //PlayerMech removedPlayerMech = removedObject as PlayerMech;
            // Итеративный поиск
            for (int i = 0; i < battleQueue.Count; i++)
            {
                if (removedObject.Equals(battleQueue[i]))    // Объект найден в очереди
                {
                    battleQueue.RemoveAt(i);                // Удаление найденного объекта
                    playerMechCount -= 1;                   // На одного меха меньше
                    playersAndFremensList.Remove(removedObject as PlayerMech);   // Удаляем мех из списка, чтобы враги в него не стреляли
#pragma warning disable CS1030 // #warning: "Здесь может закрасться ошибка. Надо все просчитать внимательно!!!'
#warning Здесь может закрасться ошибка. Надо все просчитать внимательно!!!
                    // Правим отсечку раунда, если удаляемый объект еще не выполнил свой ход в текущем раунде
                    if (i < cutOfRound)
#pragma warning restore CS1030 // #warning: "Здесь может закрасться ошибка. Надо все просчитать внимательно!!!'
                    {
                        cutOfRound -= 1;
                    }
                    //DetermineEndBattle();
                    // UnityEngine.Debug.Log("Удален из очереди объект: " + removedObject.ToString());
                    break;
                }
            }
            // Обнуляем указатель
            //removedObject = null;
        }
        
        if(DetermineEndBattle()) // Бой закончен
        {
            if (!battleIsFinishing) //препятствуем множественному вызову
            {
                EndBattle();
            }

            return true;
        }
        else // Бой продолжается
        {
            return false;
        }
    }

    public void DisableMechsOutline()
    {
        foreach (object obj in battleQueue)
        {
              if (obj is Enemy)
              {
                  Enemy _enemy = (obj as Enemy);
                  BodyOutliner _bodyOutliner = _enemy.GetComponent<BodyOutliner>();
                  if (_bodyOutliner != null)
                  {
                      _bodyOutliner.DisableOutline();
                  }
              }
              if (obj is PlayerMech)
              {
                  PlayerMech _playerMech = (obj as PlayerMech);
                  BodyOutliner _bodyOutliner = _playerMech.GetComponent<BodyOutliner>();
                  if (_bodyOutliner != null)
                  {
                      _bodyOutliner.DisableOutline();
                  }
              }
        }
    }

    public List<IRegularble> GetPlayerMechsAndFremensList()
    {
        return playersAndFremensList;
    }
    
    public List<IRegularble> GetEnemiesList()
    {
    	return enemiesList;
    }

    public ArrayList GetBattleQueue()
    {
        return battleQueue;
    }

    /// <summary>
    /// Добавление участников сражения
    /// </summary>
    /// <param name="removedObjects">Список участников на добавление</param>
    public void InsertObjectToBattleQueue(ArrayList insertedObject)
    {

    }

    /// <summary>
    /// Обработчик конца хода. Вызывается участниками сражения
    /// </summary>
    public void EndOfStep()
    {
        // Уменьшаем отсечку на 1
        cutOfRound -= 1;
        object tmpObj;

        // Проверка конца раунда
        if (cutOfRound == 0)
        {
            tmpObj = battleQueue[0];     // Перемещение первого участника в конец очереди
            battleQueue.Add(tmpObj);
            battleQueue.RemoveAt(0);

            //BattleInterfaceManager.Instance.turnLabel.ShowTurn();
            LevelHandler.Instance.RoundEnd(); // Сообщаем менеджеру уровня, что раунд закончился (он проверит активные эффекты на земле)
            PrepareBattleQueueForNextRound();
            return;
        }

        tmpObj = battleQueue[0];     // Перемещение первого участника в конец очереди
        battleQueue.Add(tmpObj);
        battleQueue.RemoveAt(0);
        //roundCount += 1;

   //     F3DTime.time.AddTimer(2f, 1, Step);
        Step();
    }

    /// <summary>
    /// Метод записывает очередь боя
    /// </summary>
    /// <param name="btlQueue">Список участников, из которых необходимо создать очередь</param>
    public void SetBattleQueue(ArrayList btlQueue)
    {
        Debug.Log("Battle Manager: Начало боя!");
        UnityEngine.Debug.Log("Battle Manager: Кол-во живых объектов, подлежащих уничтожению = " + destroyedObjects.Count);
        // Очистка массива уничтожаемых объектов
        destroyedObjects.Clear();
        // Очистка массива уничтожаемых трупов бывших объектов
        destroyedPrefabs.Clear();
        // Сброс счетчиков
        enemyCount = 0;
        playerMechCount = 0;
        // Запись указателя на список участников
        battleQueue = btlQueue;
        // Копирование указателей на учатсников в отдельный массив (для удаления со сцены, после боя)
        destroyedObjects = (ArrayList)btlQueue.Clone();
        
        // Сортировка по инициативе
        battleQueue.Sort();
        allPlayerMechList.Clear(); // очищаем то, что осталось от предидущего боя
        foreach (object obj in battleQueue) // считаем сколько всего было игроков
        {
            if (obj is PlayerMech)
            {
                allPlayerMechList.Add(obj as PlayerMech);
            }
        }
        DevConsole.singleton.Log("Начало боя!");
        cutOfRound = battleQueue.Count; // Задаем отсечку раунда
        MakeMechsSnapshots(); // сохраняем исходные состояния мехов
        CalculateEnemyAndPlayerCount(); // Определение кол-ва врагов и мехов игрока
        Step();                         // Первый ход, первого раунда!
    }

    // Метод добавляет указатель на объект уничтоженного танка Enemy.DestroyMech() и PlayerMech.DestroyMech()
    public void AddDestroyedPrefab(GameObject go)
    {
        destroyedPrefabs.Add(go);
    }

    #endregion

    #region battleTester
    public PlayerMech GetPlayerMech()
    {
        return allPlayerMechList[0];
    }

    public Enemy GetEnemy()
    {
        foreach (object obj in battleQueue)
        {
            if (obj is Enemy)
            {
                return obj as Enemy;
            }
        }
        return null;
    }
    #endregion
}
