using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Database : MonoBehaviour
{
    List<Weapon> weaponsList = new List<Weapon>();

    // Start is called before the first frame update
    void Start()
    {
        InitializeWeaponsList();
    }

    void InitializeWeaponsList()
    {
        Weapon newWeapon = new Weapon("Dagger", 1, 3, 1.0f);
        weaponsList.Add(newWeapon);

        newWeapon = new Weapon("Short Sword", 2, 4, 1.0f);
        weaponsList.Add(newWeapon);

        newWeapon = new Weapon("Long Sword", 3, 5, 1.5f);
        weaponsList.Add(newWeapon);

        newWeapon = new Weapon("Rapier", 2, 3, 1.0f);
        weaponsList.Add(newWeapon);

        newWeapon = new Weapon("Hammer", 2, 6, 1.8f);
        weaponsList.Add(newWeapon);

        newWeapon = new Weapon("Axe", 1, 6, 2.0f);
        weaponsList.Add(newWeapon);

        newWeapon = new Weapon("Mace", 1, 4, 1.0f);
        weaponsList.Add(newWeapon);
    }
}
