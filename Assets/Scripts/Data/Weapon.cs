using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon
{
    string name;
    int minDamage;
    int maxDamage;
    float critRatio;

    public Weapon(string name, int minDamage, int maxDamage, float critRatio)
    {
        this.name = name;
        this.minDamage = minDamage;
        this.maxDamage = maxDamage;
        this.critRatio = critRatio;
    }
}
